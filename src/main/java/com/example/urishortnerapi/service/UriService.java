package com.example.urishortnerapi.service;

import com.example.urishortnerapi.model.entity.Uri;
import com.example.urishortnerapi.repository.UriRepository;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ExecutionException;

@Service
public class UriService {

    private final UriRepository uriRepository;

    public UriService(UriRepository uriRepository) {
        this.uriRepository = uriRepository;
    }

    public Mono<String> convertToShortUri(Uri uriEntity) {
        return uriRepository.insert(uriEntity)
                .map(p -> "short-uri." + p.getId());
    }

    public Mono<String> getOriginalUri(String shortUri) {
        return Mono.just(shortUri)
                .map(r -> r.split("[.]")[1])
                .map(this::checkObjectId)
                .flatMap(uriRepository::findById)
                .switchIfEmpty(Mono.error(new EntityNotFoundException(HttpStatus.NOT_FOUND, "There is no url found with " + shortUri)))
                .flatMap(r -> {
                    if (ChronoUnit.MINUTES.between(r.getCreatedAt(), LocalDateTime.now()) > 1) {
                        uriRepository.delete(r).subscribe();
                        throw new EntityNotFoundException(HttpStatus.BAD_REQUEST, "LinkExpired!");
                    }
                    return Mono.just(r);
                })
                .map(Uri::getLongUri);
    }

    private ObjectId checkObjectId(String id) {
        if (ObjectId.isValid(id)) {
            return new ObjectId(id);
        } else {
            throw new EntityNotFoundException(HttpStatus.BAD_REQUEST, "Invalid URI!");
        }
    }

}
