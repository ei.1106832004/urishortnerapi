package com.example.urishortnerapi.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public final class EntityNotFoundException extends ResponseStatusException {

    public EntityNotFoundException(HttpStatus status) {
        super(status);
    }

    public EntityNotFoundException(HttpStatus status, String reason) {
        super(status, reason);
    }
}
