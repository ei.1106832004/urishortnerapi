package com.example.urishortnerapi.controller;

import com.example.urishortnerapi.mapper.UriMapper;
import com.example.urishortnerapi.model.dto.UriRequestDto;
import com.example.urishortnerapi.service.UriService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.net.URI;

@RestController
@RequestMapping("/api/v1")
public class UriController {

    private final UriService uriService;

    public UriController(UriService uriService) {
        this.uriService = uriService;
    }

    @PostMapping("create-short")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<String> createShortUri(@RequestBody UriRequestDto request) {
        return Mono.just(request)
                .map(UriMapper::toEntity)
                .flatMap(uriService::convertToShortUri);
    }

    @GetMapping(value = "{shortUri}")
    @Cacheable(value = "urls", key = "#shortUri", sync = true)
    public Mono<ResponseEntity<Void>> getAndRedirect(@PathVariable String shortUri) {
        return Mono.just(shortUri)
                .flatMap(uriService::getOriginalUri)
                .map(s -> ResponseEntity.status(HttpStatus.FOUND)
                        .location(URI.create(s))
                        .build());
    }

}
