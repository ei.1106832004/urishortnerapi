package com.example.urishortnerapi.repository;

import com.example.urishortnerapi.model.entity.Uri;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UriRepository extends ReactiveMongoRepository<Uri, ObjectId> {
}
