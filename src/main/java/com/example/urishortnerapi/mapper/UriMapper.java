package com.example.urishortnerapi.mapper;

import com.example.urishortnerapi.model.dto.UriRequestDto;
import com.example.urishortnerapi.model.entity.Uri;

public final class UriMapper {

    private UriMapper() {
    }

    public static Uri toEntity(UriRequestDto dto) {
        return new Uri().setLongUri(dto.getLongUri());
    }
}
