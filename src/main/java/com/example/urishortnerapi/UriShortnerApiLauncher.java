package com.example.urishortnerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication(scanBasePackages = "com.example.urishortnerapi", proxyBeanMethods = false)
@EnableMongoAuditing
@EnableCaching
public class UriShortnerApiLauncher {

    /**
     * Main method, used to run the application.
     * @param args the command line arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(UriShortnerApiLauncher.class, args);
    }

}
