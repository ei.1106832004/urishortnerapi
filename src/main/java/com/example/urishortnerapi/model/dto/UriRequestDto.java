package com.example.urishortnerapi.model.dto;

import java.util.StringJoiner;

public class UriRequestDto {

    private String longUri;

    public String getLongUri() {
        return longUri;
    }

    public UriRequestDto setLongUri(String longUri) {
        this.longUri = longUri;
        return this;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UriRequestDto.class.getSimpleName() + "[", "]")
                .add("longUri='" + longUri + "'")
                .toString();
    }
}
