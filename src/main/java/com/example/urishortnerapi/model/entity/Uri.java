package com.example.urishortnerapi.model.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
public class Uri {

    @Id
    private ObjectId id;

    private String longUri;

    @CreatedDate
    private LocalDateTime createdAt;

    public ObjectId getId() {
        return id;
    }

    public Uri setId(ObjectId id) {
        this.id = id;
        return this;
    }

    public String getLongUri() {
        return longUri;
    }

    public Uri setLongUri(String longUri) {
        this.longUri = longUri;
        return this;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Uri setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

}
